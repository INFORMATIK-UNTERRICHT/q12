
public class ZehnerSpeicher{
    // Instanzvariablen - ersetzen Sie das folgende Beispiel mit Ihren Variablen
    private Speicher[] speicherBank;
    private int anzahlBelegteBaenke;

    public ZehnerSpeicher(){
        speicherBank = new Speicher[10];
        for(int i = 0;  i < 10 ; i++){
            speicherBank[i] = new Speicher();
        }
        this.anzahlBelegteBaenke = 0;
    }

    public synchronized void ablegen(Kiste kiste){ 
        if(this.anzahlBelegteBaenke<10){
            speicherBank[anzahlBelegteBaenke].flipFrei();
            speicherBank[anzahlBelegteBaenke].setKiste(kiste);
            System.out.println("Kiste produziert mit Nummer " + kiste.getNummer());
            System.out.println("Kiste abgelegt in Speicherbank Nummer " + this.anzahlBelegteBaenke);
            this.anzahlBelegteBaenke++;
            this.notifyAll();
        }
        else{
            System.out.println("Speicherbank ist vollständig belegt. Erzeuger muss warten.");                
            try{
                this.wait();
            }
            catch(Exception e){

            }
        }
    }

    public synchronized Kiste abholen(){
        if(this.anzahlBelegteBaenke>0){
            this.anzahlBelegteBaenke--;
            speicherBank[this.anzahlBelegteBaenke].flipFrei();
            System.out.println("Kiste abgeholt mit Nummer " + speicherBank[this.anzahlBelegteBaenke].getKiste().getNummer());
            System.out.println("Kiste abgeholt von Speicherbank Nummer " + this.anzahlBelegteBaenke);
            this.notifyAll();
            return speicherBank[this.anzahlBelegteBaenke+1].getKiste();                
        }           
        else{
            System.out.println("Speicherbank hat keine Kiste. Verbraucher muss warten.");                    
            try{
                this.wait();
            }
            catch(Exception e){

            }
            return null;
        }
    }
}

