public class Kiste{
    // Attribute
    private int nummer;
    // Attribute Ende
    // ---------------------------------------------
    // Konstruktoren
    public Kiste(int nummer){
        this.nummer = nummer;
    }
    // Konstruktoren Ende
    // ---------------------------------------------
    // Methoden
    public int getNummer(){
        return nummer;
    }
    // Methoden Ende
    // ---------------------------------------------

}
