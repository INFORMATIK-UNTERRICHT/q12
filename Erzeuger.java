import java.util.Date;

public class Erzeuger implements Runnable{
    // Attribute
    private ZehnerSpeicher speicher;
    private int nummer;
    private Date date = new Date();
    // Attribute Ende
    // ---------------------------------------------
    // Konstruktoren
    public Erzeuger(ZehnerSpeicher speicher, int nummer){
        this.speicher = speicher;
        this.nummer = nummer;
    }
    // Konstruktoren Ende
    // ---------------------------------------------
    // Methoden
    public void run() {
        for(int i = 0 ; i < 100 ; i++){
            speicher.ablegen(produzieren(i));
        }
    }

    public Kiste produzieren(int kistenNummer){
        try{
           Thread.currentThread().sleep(500);
        }
        catch(Exception e){
        }   
        return new Kiste(kistenNummer);
    }
    // Methoden Ende
    // ---------------------------------------------

}
