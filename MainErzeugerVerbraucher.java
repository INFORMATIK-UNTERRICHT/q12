public class MainErzeugerVerbraucher{
    // Attribute

    // Attribute Ende
    // ---------------------------------------------
    // Konstruktoren
    public MainErzeugerVerbraucher(){

    }
    // Konstruktoren Ende
    // ---------------------------------------------
    // Methoden
    public static void main(String[] args){
        ZehnerSpeicher speicher = new ZehnerSpeicher();
        Erzeuger erzeuger = new Erzeuger(speicher,1);
        Verbraucher verbraucher = new Verbraucher(speicher);
        Thread thread1 = new Thread(erzeuger);
        Thread thread2 = new Thread(verbraucher);
        thread1.start();
        thread2.start();
    }
    // Methoden Ende
}
