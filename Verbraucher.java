import java.util.Random;

public class Verbraucher implements Runnable{
    // Attribute
    private ZehnerSpeicher speicher;
    private Random zufall = new Random();
    // Attribute Ende
    // ---------------------------------------------
    // Konstruktoren
    public Verbraucher(ZehnerSpeicher speicher){
        this.speicher = speicher;
    }
    // Konstruktoren Ende
    // ---------------------------------------------
    // Methoden
    public void run() {
        for(int i = 0 ; i < 100 ; i++){
            this.einlagern();
        }
    }

    public void einlagern(){
        speicher.abholen();
        try{
            Thread.currentThread().sleep(zufall.nextInt(1000)+500);
        }
        catch(Exception e){

        }  
    }
    // Methoden Ende
    // ---------------------------------------------
}
