public class Speicher {
    // Attribute
    private boolean frei = true;
    private Kiste kiste = null;
    // Attribute Ende
    // ---------------------------------------------
    // Konstruktoren
    public Speicher(){
        // passiert weiter erstmal nix
    }
    // Konstruktoren Ende
    // ---------------------------------------------
    // Methoden
    public boolean getFrei(){
        return this.frei;
    }
    
    public void flipFrei(){
        this.frei = !(this.frei);
    }
    
    public void setKiste(Kiste kiste){
        this.kiste = kiste;
    }
    
    public Kiste getKiste(){
        return this.kiste;
    }
    
    
    public void ablegen(Kiste kiste){
        this.frei = false;
        this.kiste = kiste;
        System.out.println("Kiste produziert mit Nummer " + this.kiste.getNummer());   
    }

    public Kiste abholen(){
        this.frei = true;
        System.out.println("Kiste eingelagert mit Nummer " + this.kiste.getNummer());   
        return this.kiste; 
    }
    // Methoden Ende
    // ---------------------------------------------
}
